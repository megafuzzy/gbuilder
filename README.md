# Project description

A comprehensive solution to managing all customer related tasks in one window; material options and configurations, change requests and extra works, communications, documentation, defect management and much more.

Optimise office tasks to save time and resources, create an unparalleled customer experience and deliver accurate, real-time data to the construction site to eliminate mistakes.

www.gbuilder.com

**I do not own IPR for the commercial code of GBuilder core so only some old screenshots have been added to this repository**

# My role in development and technologies used

- I worked as the CTO, lead developer and first employee of GBuilder during years 2012-2016.
- Due to my background as a Java developer, Vaadin was chosen as the framework in the beginning.
- I wrote the first version of the system before I was assigned an offshore team from Bangladesh and my role changed to being mainly a team leader, lead developer project manager and UI designer.
- UI for all the screenshots was designed by me, and implemented using Vaadin themes and components.
- Glassfish was used as the server and MySQL for db, Jenkins was used for CI/deployment.